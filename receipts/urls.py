from django.urls import path
from receipts.views import get_receipts, create_receipt, category_list, account_list, create_category, create_account


urlpatterns = [
    path("", get_receipts, name="home"),
    path("create/", create_receipt, name="create"),
    path("categories/", category_list, name="categories"),
    path("categories/create", create_category, name="create_category"),
    path("accounts/", account_list, name="accounts"),
    path("accounts/create", create_account, name="create_account"),
]
